const database = require("./dbService");
const complimentsArr = require("./compliment");
const jokeArr = require("./joke");
const { dickPic } = require("./dickpickService");

const init = () => database.initData();

const initGame = ({ sendMessage, id }) => {
  let db = database.read(id);

  if (database.isEmpty(id)) {
    db = { users: [], lastMessage: null };

    sendMessage(
      `Дарова, ебанаты. Я - бот, который будет следить за ростом ваших залуп`
    );

    database.save(db, id);
  } else {
    sendMessage(
      `Долбаёб? я и так уже занят измирением ваших залуп. Заебешь, идиот сука конченый я твой рот галимый ебал. Мать твоя шалава ебаная сукаа колхозник бьять тупой нахуй пиздец я в ахуе нахуй ты блять существуешь придурок блять сука уебан нахуй были бы у меня руки я бы тебя нахуй бы забил до смерти гнида блять`
    );
  }
};

const joinGame = ({ sendMessage, id, username }) => {
  const db = database.read(id);

  if (database.isEmpty(id)) {
    return initGame({ sendMessage, id });
  }

  if (
    !database.isEmpty(id) &&
    !db.users.find((item) => item.username === username)
  ) {
    db.users.push({ username, dickSize: "0.1" });

    sendMessage(`Ты вступил в клуб большых членов, @${username}`);

    database.save(db, id);
  } else {
    sendMessage(`Ты и так уже учавствуешь в игре, @${username}!`);
  }
};

const readStats = ({ sendMessage, id }) => {
  const db = database.read(id);

  if (!database.isEmpty(id) && db.users.length > 0) {
    let res = "Шо ж, на данный момент ваши залупы выглядят так:\n";

    db.users.sort((a, b) => Number(b.dickSize) - Number(a.dickSize));
    db.users.map((item, index) => {
      res += `\n${index + 1}. <b>${item.username}</b> - ${item.dickSize}см`;
    });

    sendMessage(res);
  } else {
    sendMessage("Ну и кому мне мерять хуй????");
  }
};

const measureDick = ({ sendMessage, id }) => {
  const db = database.read(id);

  if (!database.isEmpty(id) && db.users.length > 0) {
    const { lastMessage } = db;

    if (lastMessage === new Date().toLocaleDateString()) {
      sendMessage("Вы уже сегодня наростили свои залупы");
    } else {
      const winnerIndex = Math.floor(Math.random() * db.users.length);
      const size = Math.random().toFixed(2);
      const newSize = Number(db.users[winnerIndex].dickSize) + Number(size);

      // update dick size
      db.users[winnerIndex].dickSize = newSize.toFixed(2);
      sendMessage(
        `Поздравим @${db.users[winnerIndex].username}. У него залупа выросла на ${size}см!`
      );

      // update last message date
      db.lastMessage = new Date().toLocaleDateString();

      database.save(db, id);
    }
  } else {
    sendMessage("Некому наращивать писю....");
  }
};

const sendDickPic = ({ sendMessage, id, username, bot }) => {
  const db = database.read(id);

  const user = db.users.find((item) => item.username === username);
  const picture = dickPic(user.dickSize);

  sendMessage(
    `@${username}, твой болт размером в ${user.dickSize}см выглядит так:`
  );
  setTimeout(() => bot.sendPhoto(id, picture), 1000);
};

const sendDatabase = ({ sendMessage, text, id, bot }) => {
  const password = text.split(" ")[1];
  if (password === process.env.DB_PASSWORD) {
    bot.sendDocument(id, database.location);
  } else sendMessage("та пошел ты на хуй?");
};

const sendCompliment = ({ sendMessage, username, text }) => {
  const compliment =
    complimentsArr[Math.floor(Math.random() * complimentsArr.length)];
  const target = text.split(" хуесосина")[0];

  target
    ? sendMessage(`${target}, ${compliment}`)
    : sendMessage(`${username} долбаеб ебаный`);
};

const sendJoke = ({ sendMessage, id, bot }) => {
  jokeArr.map((item, index) =>
    setTimeout(() => sendMessage(item), 2000 * index)
  );

  setTimeout(
    () =>
      bot.sendPhoto(
        id,
        "https://i.pinimg.com/originals/50/10/2b/50102b765fbfe102017e9f28b0735f44.jpg"
      ),
    10000
  );
};

module.exports = {
  joinGame,
  initGame,
  sendJoke,
  readStats,
  measureDick,
  sendDickPic,
  sendDatabase,
  sendCompliment,
  init,
};
