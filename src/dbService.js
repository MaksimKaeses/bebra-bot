const fs = require("fs");
const location = "./db/db.json";

const initData = () => {
  try {
    const data = JSON.parse(fs.readFileSync(location));
    return data;
  } catch (err) {
    throw Error(err);
  }
};

const read = (id) => {
  let data = initData();
  return data[id] ? data[id] : {};
};

const save = (data, id) => {
  const init = initData();
  init[id] = data;
  fs.writeFileSync(location, JSON.stringify(init));
};

const isEmpty = (id) => {
  return read(id) ? Object.keys(read(id)).length <= 0 : false;
};

module.exports = {
  initData,
  read,
  save,
  isEmpty,
  location,
};
