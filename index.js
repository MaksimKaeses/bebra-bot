const TelegramBot = require("node-telegram-bot-api");
const bot = new TelegramBot(process.env.TOKEN, { polling: true });
const services = require("./src/services");

try {
  services.init();
} catch (e) {
  console.error(`\n${e}\nNo database has been found`);
  process.exit();
}

const responde = (msg, callback) => {
  const { id } = msg.chat;
  const { text } = msg;
  let { username, first_name } = msg.from;
  username = username || first_name;

  const sendMessage = (messageText) =>
    bot.sendMessage(id, messageText, { parse_mode: "HTML" });

  if (typeof callback === "string") {
    services[callback]
      ? services[callback]({ sendMessage, text, id, username, bot })
      : console.error("Service is not found");
  } else sendMessage(callback({ username }));
};

bot.onText(/\/testcommand/, (msg) =>
  responde(msg, () => `Та пішов ти на хуй чорт попущений`)
);
bot.onText(/\/donate/, (msg) =>
  responde(
    msg,
    ({ username }) =>
      `@${username}, стыдно? \n\n5375414117731207 \n1см = 10грн.`
  )
);
bot.onText(/\/joke/, (msg) => responde(msg, "sendJoke"));
bot.onText(/\/starthuy/, (msg) => responde(msg, "initGame"));
bot.onText(/\/join/, (msg) => responde(msg, "joinGame"));
bot.onText(/\/huystats/, (msg) => responde(msg, "readStats"));
bot.onText(/\/growhuy/, (msg) => responde(msg, "measureDick"));
bot.onText(/\/myhuy/, (msg) => responde(msg, "sendDickPic"));
bot.onText(/\/sendcurrentdatabase/, (msg) => responde(msg, "sendDatabase"));
bot.onText(/хуесосина/, (msg) => responde(msg, "sendCompliment"));

bot.on("polling_error", (err) => {
  console.error(err);
});
